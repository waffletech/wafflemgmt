import os
import subprocess

from flask import request, Blueprint
from marshmallow import Schema, fields
import dns.resolver as dns
import peewee as pw

from wafflesrv import flask_db, config, J_ENV
from wafflesrv.auth import require_auth, require_admin
from wafflesrv.user import Model as UserModel
from wafflesrv.util import reload_unit, Record


# Web API DB model
class Model(flask_db.Model):
    user = pw.ForeignKeyField(UserModel)
    domain = pw.CharField(primary_key=True)


api = Blueprint('web', __name__)


# TODO Test
@api.route('/web/domains', methods=['GET'])
@require_auth
def web_domains_get(auth_user=None):
    class Rsp(Schema):
        domains = fields.List(fields.String())

    doms = [entry.domain for entry in Model.get(user=auth_user.model)]
    return Rsp().dump({'domains': doms})


# TODO Test
@api.route('/web/domains', methods=['POST'])
@require_auth
def web_domains_post(auth_user=None):
    class Req(Schema):
        domain = fields.String()

    class Rsp(Schema):
        success = fields.Bool()
        info = fields.String()

    # check if domain exists
    req = Req(request.json['text'])
    if Model.select(domain=req.domain):
        return Rsp().dump({
            'success': False,
            'info': 'Domain "{}" in use by another member.'.format(req.domain)
        })

    req_records = (
        Record('{}', 'A', config['web']['a_record']),
        Record('{}', 'AAAA', config['web']['aaaa_record']),
    )

    # verify valid domain & correct record
    info = ''
    for rec in req_records:
        try:
            query = dns.query(rec.rr.format(req.domain), req.type)
            if rec.text != query.to_text():
                info += 'RR "{}" is wrong, expected "{}".\n'.format(rec.rr,
                                                                    rec.text)
        except dns.NoAnswer:
            info += 'RR "{}" is missing.\n'.format(rec.rr)
    if info:
        return Rsp().dump({'success': False, 'info': info})

    # setup certbot
    if not setup_ssl(req.domain):
        return Rsp().dump({'success': False, 'info': 'TODO'})

    # add domain
    Model.insert(user=auth_user.model, domain=req.domain)

    # reconfigure nginx
    reconfigure()

    return Rsp().dump({'success': True, 'info': ''})


# TODO Test
@api.route('/web/domains', methods=['DELETE'])
@require_auth
def web_domains_delete(auth_user=None):
    class Req(Schema):
        domain = fields.String()

    class Rsp(Schema):
        success = fields.Bool()
        info = fields.String()

    # check if domain registered to user
    req = Req(request.json['text'])

    try:
        dom = Model.get(user=auth_user.model, domain=req.domain)
    except:
        return Rsp().dump({
            'success': False,
            'info': 'Domain "{}" not used by "{}".'.format(req.domain,
                                                           auth_user.username)
        })

    # teardown certbot
    teardown_ssl(req.domain)

    # remove domain
    dom.delete()

    # reconfigure nginx
    reconfigure()

    return Rsp().dump({'success': True, 'info': ''})


# TODO Test
# @api.route('/admin/web/domains')
# @require_admin()
# def all_domains(auth_user=None):
#     class DomEntry(Schema):
#         username = fields.String()
#         domain = fields.String()
#     class Rsp(Schema):
#         domains = fields.List(fields.Nested(DomEntry()))
#
#     # return all domains
#     doms = [{'user': entry.user.uid, 'domain': entry.domain}
#             for entry in Model.select()]
#     return Rsp().dump({'domains': doms})


# TODO Implement
def teardown_ssl(domain):
    pass


def setup_ssl(domain):
    cert_path = '/etc/letsencrypt/live/{}/fullchain.pem'.format(domain)
    if os.path.exists(cert_path):
        # TODO Is this right?
        return True

    # Now we actually talk to LE
    print('Attempting to obtain a TLS certificate for this domain...')
    cbot_cmd = ('/usr/bin/certbot certonly'
                '-n --webroot --webroot-path /var/www/well-known'
                '-d {}').format(domain)
    try:
        subprocess.run(cbot_cmd.split(), check=True)
    except:
        return False

    # Now we check if LE actually worked
    return os.path.exists(cert_path)


def reconfigure():
    template = J_ENV.get_template('nginx.conf.tpl')

    for entry in Model:
        tvars = {
            'user': entry.user.username(),
            'domain': entry.domain,
            'tls': True
        }
        with open(config['web']['nginx_conf'], 'w') as conf:
            conf.write(template.render(tvars))

    reload_unit('nginx')
