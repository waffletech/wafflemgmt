from functools import wraps


class FakeOidc():
    def __init__(self):
        self.authorized = False

    def require_login(self, view_func):
        @wraps(view_func)
        def wrapper(*args, **kwargs):
            if not self.authorized:
                return False
            return view_func(*args, **kwargs)

        return wrapper
