from collections import namedtuple

import peewee as pw

from wafflesrv import config, flask_db


class Model(flask_db.Model):
    username = pw.CharField()


class User():
    def __init__(self, oidc):
        self._oidc = oidc
        self._model = Model.get_or_create(
                username=oidc.user_getfield('username'))

    @property
    def model(self):
        return self._model

    @property
    def name(self):
        return self._oidc.user_getfield('name')

    @property
    def username(self):
        return self._model.username

    @property
    def uid(self):
        return self._oidc.user_getfield('uid')

    @property
    def gid(self):
        return self._oidc.user_getfield('gid')

    @property
    def groups(self):
        return self._oidc.user_getfield('groups')
