'''wafflesrv

Usage:
    wafflesrv run [--config=FILE] [--mail] [--web]
    wafflesrv -h | --help
    wafflesrv --version

Options:
    -h --help        Show this help and exit.
    --version        Show the version info.
    --config=FILE    Config file path [default: config.ini].
    --mail           Enable the mail API module.
    --web            Enable the website API module.

'''

from docopt import docopt

from wafflesrv import app, config, oidc, flask_db
import wafflesrv.api.mail as mail
import wafflesrv.api.web as web


@app.route('/hello')
def test_hello():
    return 'Hello, world!'


def run():
    args = docopt(__doc__, version='0.1')

    if args['--mail']:
        app.register_blueprint(mail.api)
    if args['--web']:
        app.register_blueprint(web.api)

    # setup config
    app.config.from_object(__name__)

    config.read(args['--config'])

    if config.has_section('flask'):
        app.config.from_mapping(config['flask'])
    if config.has_section('oidc'):
        app.config.from_mapping(config['oidc'])

    # init flask extensions
    flask_db.init_app(app)
    oidc.init_app(app)

    # create db tables
    models = flask_db.Model.__subclasses__()
    flask_db.database.create_tables(models)

    # run flask
    app.run()


if __name__ == '__main__':
    run()
