import unittest
from unittest.mock import patch, MagicMock

from flask import Response

from test.fake_oidc import FakeOidc


def patch_imports(func):
    def wrapper(self, *args, **kwargs):
        with patch.dict('sys.modules', {'wafflesrv.user': self.mock_user}):
            func(self)
    return wrapper


class TestAuth(unittest.TestCase):
    def setUp(self):
        self.fake_oidc = FakeOidc()
        self.fake_oidc.authorized = True

        self.mock_user = MagicMock()
        self.mock_user.User.return_value = self.mock_user
        self.mock_user.username = 'ghopper'
        self.mock_user.name = 'Grace'
        self.mock_user.groups = ['alpha', 'beta']
        self.mock_user.uid = 1000

        self.view_func = MagicMock()
        self.view_func.return_value = True

    @patch_imports
    def test_require_auth(self):
        from wafflesrv.auth import require_auth

        # test view_func(*args, auth_user=User, **kwargs) called when auth'd
        wrapper = require_auth(self.view_func, _oidc=self.fake_oidc)
        wrapper(0, neat=True)
        self.view_func.assert_called_once_with(0, neat=True,
                                               auth_user=self.mock_user)

        self.view_func.reset_mock()

        # test that view_func(...) is *not* called when not auth'd
        self.fake_oidc.authorized = False
        wrapper(0, neat=True)
        self.view_func.assert_not_called()

    @patch_imports
    def test_require_user(self):
        from wafflesrv.auth import require_user

        # test authorized user
        wrapper = require_user('ghopper', _oidc=self.fake_oidc)
        response = wrapper(self.view_func)(0, neat=True)
        self.assertEqual(response, True)
        self.view_func.assert_called_with(0, neat=True,
                                          auth_user=self.mock_user)
        self.mock_user.User.assert_called_with(self.fake_oidc)

        self.view_func.reset_mock()

        # test that view_func(...) is *not* called when not auth'd
        self.fake_oidc.authorized = False
        wrapper = require_user('ghopper', _oidc=self.fake_oidc)
        response = wrapper(self.view_func)(0, neat=True)
        self.assertFalse(response)
        self.view_func.assert_not_called()

    @patch_imports
    def test_require_group(self):
        from wafflesrv.auth import require_group

        # test a valid group
        wrapper = require_group('beta', self.fake_oidc)
        response = wrapper(self.view_func)(0, neat=True)
        self.assertEqual(response, True)
        self.view_func.assert_called_with(0, neat=True,
                                          auth_user=self.mock_user)
        self.mock_user.User.assert_called_with(self.fake_oidc)

        self.view_func.reset_mock()

        # test that view_func(...) is *not* called when not auth'd
        self.fake_oidc.authorized = True
        wrapper = require_group('delta', _oidc=self.fake_oidc)
        response = wrapper(self.view_func)(0, neat=True)
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status_code, 500)
        self.view_func.assert_not_called()

    @patch_imports
    def test_require_admin(self, mock_user=None):
        # TODO (ljencka): write this test
        pass


if __name__ == '__main__':
    unittest.main()
