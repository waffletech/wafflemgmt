from functools import wraps

from wafflesrv import config, oidc
from wafflesrv.user import User
from flask import Response


def require_auth(view_func, _oidc=oidc):
    @wraps(view_func)
    @_oidc.require_login
    def wrapper(*args, **kwargs):
        return view_func(*args, auth_user=User(_oidc), **kwargs)

    return wrapper


def require_user(username, _oidc=oidc):
    def decorator(view_func):
        @wraps(view_func)
        @_oidc.require_login
        def wrapper(*args, **kwargs):
            user = User(_oidc)
            if user.username != username:
                # TODO (ljencka): log here
                return Response(status=500)

            return view_func(*args, auth_user=user, **kwargs)
        return wrapper
    return decorator


def require_group(group, _oidc=oidc):
    def decorator(view_func):
        @wraps(view_func)
        @_oidc.require_login
        def wrapper(*args, **kwargs):
            user = User(_oidc)
            if group not in user.groups:
                # TODO (ljencka): log here
                return Response(status=500)

            return view_func(*args, auth_user=user, **kwargs)
        return wrapper
    return decorator


def require_admin(_oidc=oidc):
    return require_group(_oidc, config['wafflesrv']['admin_group'])
