import 'buefy/dist/buefy.css';
import '@mdi/font/css/materialdesignicons.css';

import Vue from 'vue';
import Buefy from 'buefy';

Vue.use(Buefy);

import Mail from './mail.vue';
import Web from './web.vue';

let app = new Vue({
  el: '#root',
  components: { Mail, Web}
})
