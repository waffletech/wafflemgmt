import configparser
from flask import Flask
from flask_oidc_ext import OpenIDConnect
import jinja2
from playhouse.flask_utils import FlaskDB

app = Flask(__name__)
oidc = OpenIDConnect()
flask_db = FlaskDB()
config = configparser.ConfigParser()
config.optionxform = str
J_ENV = jinja2.Environment(loader=jinja2.PackageLoader("wafflesrv"))
