from collections import namedtuple
import subprocess

Record = namedtuple('Record', ['rr', 'type', 'text'])

def reload_unit(name):
    "Reload a systemd unit"
    subprocess.call(["systemctl", "reload", name])
