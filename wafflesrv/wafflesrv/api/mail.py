import subprocess

from flask import request, Blueprint
from marshmallow import Schema, fields
import dns.resolver as dns
import peewee as pw

from wafflesrv import flask_db, config, J_ENV
from wafflesrv.auth import require_auth, require_admin
from wafflesrv.user import Model as UserModel
from wafflesrv.util import reload_unit, Record


# Mail API DB model
class Model(flask_db.Model):
    user = pw.ForeignKeyField(UserModel)
    domain = pw.CharField(primary_key=True)


api = Blueprint('web', __name__)


# TODO Test
@api.route('/mail/domains', methods=['GET'])
@require_auth
def mail_domains_get(auth_user=None):
    class Rsp(Schema):
        domains = fields.List(fields.String())

    doms = [entry.domain for entry in Model.get(user=auth_user.model)]
    return Rsp().dump({'domains': doms})


# TODO Test
@api.route('/mail/domains', methods=['POST'])
@require_auth
def mail_domains_post(auth_user=None):
    class Req(Schema):
        domain = fields.String()

    class Rsp(Schema):
        success = fields.Bool()
        info = fields.String()

    # check if domain exists
    req = Req(request.json['text'])
    if Model.select(domain=req.domain):
        return Rsp().dump({
            'success': False,
            'info': 'Domain "{}" in use by another member.'.format(req.domain)
        })

    # verify valid domain & correct record
    info = ''

    req_records = (
        Record('{}', 'MX', config['mail']['mx_record']),
        Record('{}', 'TXT', config['mail']['spf_record']),
        Record('_dmarc.{}', 'TXT', config['mail']['dmarc_record']),
        Record('mail._domainkey.{}', 'TXT', config['mail']['dkim_record'])
    )

    for rec in req_records:
        try:
            query = dns.resolver.query(rec.rr.format(req.domain), req.type)
            rr_real = set(res.strip() for res in
                          query.to_text().strip('" ').split(';'))
            rr_ideal = set(res.strip() for res in
                           rec.text.strip('" ').split(';'))
            if rr_ideal != rr_real:
                info += 'RR "{}" is wrong, expected "{}".\n'.format(rec.rr,
                                                                    rec.text)
        except dns.NoAnswer:
            info += 'RR "{}" is missing.\n'.format(rec.rr)
    if info:
        return Rsp().dump({'success': False, 'info': info})

    # add domain
    Model.insert(user=auth_user.model, domain=req.domain)

    # reconfigure
    reconfigure()

    return Rsp().dump({'success': True, 'info': ''})


# TODO Test
@api.route('/mail/domains', methods=['DELETE'])
@require_auth
def mail_domains_delete(auth_user=None):
    class Req(Schema):
        domain = fields.String()

    class Rsp(Schema):
        success = fields.Bool()
        info = fields.String()

    # check if domain registered to user
    req = Req(request.json['text'])

    try:
        dom = Model.get(user=auth_user.model, domain=req.domain)
    except:
        return Rsp().dump({
            'success': False,
            'info': 'Domain "{}" not used by "{}".'.format(req.domain,
                                                           auth_user.username)
        })

    # remove domain
    dom.delete()

    # reconfigure
    reconfigure()

    return Rsp().dump({'success': True, 'info': ''})


# @api.route('/admin/mail/domains', methods=['GET'])
# @require_admin()
# def all_domains(auth_user=None):
#     class DomEntry(Schema):
#         username = fields.String()
#         domain = fields.String()
#     class Rsp(Schema):
#         domains = fields.List(fields.Nested(DomEntry()))
#
#     # return all domains
#     doms = [{'user': entry.user.uid, 'domain': entry.domain}
#             for entry in Mail.select()]
#     return Rsp().dump({'domains': doms})


# TODO: Test
def reconfigure():
    domains = {entry.domain: entry.username() for entry in Model}

    # write MASTER_TABLE
    with open(config['mail']['master_table'], 'w+') as f:
        for dom, user in domains:
            f.write("{0}\t{1}\t{1}@waffle.tech\n".format(dom, user))

    # Bunch of steps here - first, the smtpd_sender_login_maps.pcre
    with open(config['mail']['slmap_file'], "w") as f:
        template = J_ENV.get_template("slmaps.pcre.tpl")
        f.write(template.render({"domains": domains}))

    # Now the virtual domains table
    with open(config['mail']['domains_file'], "w") as f:
        template = J_ENV.get_template("virtual_domains.tpl")
        f.write(template.render({"domains": domains}))

    # Now the virtual aliases table
    with open(config['mail']['aliases_file'], "w") as f:
        template = J_ENV.get_template("virtual_aliases.tpl")
        f.write(template.render({"domains": domains}))

    # Now we regenerate the indexes for the hash tables
    subprocess.check_call(["postmap", config['mail']['domains_file']])
    subprocess.check_call(["postmap", config['mail']['aliases_file']])

    # And finally load the new configuration
    reload_unit("postfix")
