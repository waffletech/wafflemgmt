'''waffle.tech computer god human-computer interface

Usage:
    waffle mail list
    waffle mail add [--skip-check] <domain>
    waffle mail remove <domain>
    waffle web list
    waffle web add <domain>
    waffle web remove <domain>
    waffle contribute info
    waffle contribute subscribe <amount>
    waffle contribute unsubscribe
    waffle alias list
    waffle alias add <alias>
    waffle alias remove <alias>
    waffle -h | --help
    waffle --version

Options:
   -h --help     Show this help and exit
   --version     Show the version info
   --skip-check  Skip the DNS check

'''

import argparse
from docopt import docopt
import requests
from requests_kerberos import HTTPKerberosAuth as kauth
import sys

ENDPOINT = 'https://127.0.0.1:420/'
# ENDPOINT = 'https://bravo.waffle.tech:420'


def run():
    args = docopt(__doc__, version='0.1')

    if args['mail']:
        if args['list']:
            req = requests.get(ENDPOINT + '/mail/domains', auth=kauth())
            print(req)
        elif args['add']:
            pass
        elif args['remove']:
            pass
    elif args['web']:
        if args['list']:
            pass
        elif args['add']:
            pass
        elif args['remove']:
            pass


if __name__ == '__main__':
    run()
